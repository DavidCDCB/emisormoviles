package com.example.marvel;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cafsoft.foundation.Data;

public class MainActivity extends AppCompatActivity {

    private EditText txtCharacter = null;
    private Button btnSetInfo = null;
    private EditText txtLugar = null;
    private EditText txtFecha = null;
    private EditText txtHora = null;
    private ExecutorService queue = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCharacter = findViewById(R.id.txtCharacter);
        btnSetInfo = findViewById(R.id.btnSetInfo);
        txtLugar = findViewById(R.id.txtLugar);
        txtFecha = findViewById(R.id.txtFecha);
        txtHora = findViewById(R.id.txtHora);

        btnSetInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInfo();
            }
        });

        if (queue == null) {
            queue = Executors.newSingleThreadExecutor();
        }

        getInfo();
    }


    //Envio de los datos usando la clase Envio junto con el metodo peticion
    public void setInfo() {
        final String nombre = txtCharacter.getText().toString();
        final String lugar = txtLugar.getText().toString();
        final String fecha = txtFecha.getText().toString();
        final String hora = txtHora.getText().toString();

        Envio objEnvio = new Envio(nombre,lugar,fecha,hora);
        objEnvio.setInfo();


        final Runnable r = new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("---------------------", "Datos enviados");
                        Toast toast = Toast.makeText(getApplicationContext(), "Datos Enviados", Toast.LENGTH_LONG);
                        toast.show();
                    }
                });

            }

        };


        queue.execute(r);

    }


    public void getInfo() {

        final String strURL = "https://unremoved-sediments.000webhostapp.com/server.php?almacenarDatos=no";

        Toast toast1 = Toast.makeText(getApplicationContext(), "Cargando datos...", Toast.LENGTH_SHORT);
        toast1.show();
        Log.d("Weather", strURL);

        final Runnable r = new Runnable() {
            @Override
            public void run() {
                Data data = null;
                URL url = null;

                try {
                    url = new URL(strURL);
                    data = new Data(url);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }


                if (data != null) {
                    Log.d("IMPORTANTE!!!", "-------JSON-COMPLETO:"+data.toText());
                    JSONObject info = null;
                    try {
                        //info = new JSONObject(peticion(strURL));
                        info = new JSONObject(data.toText());
                        Log.d("IMPORTANTE!!!", "--------DATO-ESPECIFICO:"+info.getString("nombre"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (info != null) {
                        String tnombre="",tLugar="",tFecha="",tHora="";


                        try {

                            tnombre = info.getString("nombre");
                            tLugar = info.getString("lugar");
                            tFecha = info.getString("fecha");
                            tHora = info.getString("hora");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final String nombre = tnombre;
                        final String lugar = tLugar;
                        final String fecha = tFecha;
                        final String hora = tHora;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txtCharacter.setText(nombre);
                                txtLugar.setText(lugar);
                                txtFecha.setText(fecha);
                                txtHora.setText(hora);
                            }
                        });

                    }
                }
            }

        };


        queue.execute(r);

    }
}



