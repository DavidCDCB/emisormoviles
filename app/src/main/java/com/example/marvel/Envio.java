package com.example.marvel;

import android.os.StrictMode;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import androidx.appcompat.app.AppCompatActivity;


public class Envio extends AppCompatActivity {

    String txtCharacter,txtLugar,txtFecha,txtHora;
    private ExecutorService queue = null;

    public Envio(String txtCharacter, String txtLugar, String txtFecha, String txtHora){
        this.txtCharacter=txtCharacter;
        this.txtLugar=txtLugar;
        this.txtFecha=txtFecha;
        this.txtHora=txtHora;

        if (queue == null) {
            queue = Executors.newSingleThreadExecutor();
        }

    }

    /**
     * Una función para hacer peticiones al servidor mucho mas corta que con la libreria del cucho
     * necesita una url para entregar el contenido de esta, en este caso el JSON llamado por el server
     * el retorno se debe convertir a un objeto JSON con JSONObject info = new JSONObject(peticion(strURL));
     * para acceder a sus contenidos.
     */
    public String peticion(String ruta){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String json = "";
        String inputLine;
        URL url = null;
        HttpURLConnection conn;

        try {
            url = new URL(ruta);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer response = new StringBuffer();
            json = "";

            while((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }

            json = response.toString();

            Log.d("IMPORTANTE", "-------JSON-COMPLETO:"+json);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    //Prepara el string de la url con los datos ingresados y los envia por GET
    public void setInfo() {
        final String nombre = this.txtCharacter;
        final String lugar = this.txtLugar;
        final String fecha = this.txtFecha;
        final String hora = this.txtHora;

        final String urlWebService = "https://unremoved-sediments.000webhostapp.com/server.php";
        final String params ="nombre="+nombre+"&fecha="+fecha+"&lugar="+lugar+"&hora="+hora+"&almacenarDatos=si";
        final String strURL = urlWebService + "?" + params;


        final Runnable r = new Runnable() {
            @Override
            public void run() {

                peticion(strURL);

            }

        };

        queue.execute(r);
    }
}
